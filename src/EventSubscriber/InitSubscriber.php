<?php /**
 * @file
 * Contains \Drupal\timelogin\EventSubscriber\InitSubscriber.
 */

namespace Drupal\timelogin\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InitSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onEvent', 0]];
  }

  public function onEvent() {
    $logged_in = \Drupal::currentUser()->isAuthenticated();
    if ($logged_in) {
      if (!timelogin_user_access()) {
        module_load_include('pages.inc', 'user');
        drupal_set_message(t('You are not authorised to access the site in this time slot.'));
        user_logout();
      }
    }
  }

}

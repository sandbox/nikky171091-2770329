<?php /**
 * @file
 * Contains \Drupal\timelogin\Controller\DefaultController.
 */

namespace Drupal\timelogin\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the timelogin module.
 */
class DefaultController extends ControllerBase {

  public function timelogin_manage_timeslot() {
    $header = [
      ['data' => t('ID'), 'field' => 'id', 'sort' => 'asc'],
      [
        'data' => t('Role'),
        'field' => 'timelogin_role_id',
      ],
      ['data' => t('From time'), 'field' => 'timelogin_from_time'],
      [
        'data' => t('To time'),
        'field' => 'timelogin_to_time',
      ],
      ['data' => t('Action')],
    ];
    $rows = [];
    $query = db_select('time_login', 'tl')->fields('tl');
    $table_sort = $query->extend('TableSort')
      ->orderByHeader($header);
    $pager = $table_sort->extend('PagerDefault')
      ->limit(10);
    $results = $pager->execute();
    $roles = user_roles();
    foreach ($results as $row) {
      // @FIXME
// l() expects a Url object, created from a route name or external URI.
// $rows[] = array($row->id,
//       $roles[$row->timelogin_role_id],
//       $row->timelogin_from_time,
//       $row->timelogin_to_time,
//       l(t('Edit'), 'admin/timelogin_timeslot/' . $row->id . '/edit') . ' | ' .
//       l(t('Delete'), 'admin/timelogin_timeslot/' . $row->id . '/delete', array('query' => array('destination' => 'admin/timelogin'))),
//     );

    }
    // @FIXME
    // theme() has been renamed to _theme() and should NEVER be called directly.
    // Calling _theme() directly can alter the expected output and potentially
    // introduce security issues (see https://www.drupal.org/node/2195739). You
    // should use renderable arrays instead.
    // 
    // 
    // @see https://www.drupal.org/node/2195739
    // return theme('table', array(
    //     'header' => $header,
    //     'rows' => $rows,
    //   )) . theme('pager');

  }

}

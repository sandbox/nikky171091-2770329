<?php

/**
 * @file
 * Contains \Drupal\timelogin\Form\TimeloginTimeslotDeleteConfirm.
 */

namespace Drupal\timelogin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TimeloginTimeslotDeleteConfirm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'timelogin_timeslot_delete_confirm';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $form['tlid'] = [
      '#type' => 'value',
      '#value' => $id,
    ];
    return confirm_form($form, t('Are you sure you want to delete this time slot?'), '', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    if ($form_state->getValue(['confirm'])) {
      timelogin_timeslot_delete($form_state->getValue(['tlid']));
      drupal_set_message(t('Specific time slot has been deleted successfully.'));
    }
  }

}
